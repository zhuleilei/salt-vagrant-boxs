# provision

为已有的box提供升级脚本，每隔一段时间，会更新一次box，中间的一些小的变更，将会通过升脚本来提供升级

## how to run
### windows
```
vagrant provision
```
or
登录虚拟机之后
```
$ wget -qO- /url/to/bash/script | sudo sh -x
```
推荐使用后面的方式，因为不排除，有些时候，存在交互的可能。

### linux
```
$ vagrant provision
```
or
登录虚拟机之后
```
$ vagrant ssh
$ wget -qO- /url/to/bash/script | sudo sh -x
```
推荐使用后面的方式，因为不排除，有些时候，存在交互的可能。