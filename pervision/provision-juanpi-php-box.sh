#!/bin/bash

check_version()
{
    if [ -f "/root/.installed/box_provision" ] 
    then
        return `cat /root/.installed/box_provision`
    else
        mkdir -p /root/.installed/
        return 0
    fi
}

log_version()
{
    echo "$1" >/root/.installed/box_provision;
}

# ---------------------------- cart 证书部署 begin --------------------------------
echo "cart 证书部署 开始..."
version=1
check_version

if [ "$?" -lt "$version" ]
then
    echo "下载证书密钥......";
    sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/1/nginx/1_cart.juanpi.com_bundle.crt' -O /usr/local/webserver/tengine/sslkey/1_cart.juanpi.com_bundle.crt
    sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/1/nginx/2_cart.juanpi.com.key' -O /usr/local/webserver/tengine/sslkey/2_cart.juanpi.com.key
    echo "更改配置文件......"
    sed -i '7i\        include ssl.conf;' /usr/local/webserver/tengine/conf/vhosts/cart.juanpi.com.conf;
    sed -i '8i\        ssl_certificate ../sslkey/1_cart.juanpi.com_bundle.crt;' /usr/local/webserver/tengine/conf/vhosts/cart.juanpi.com.conf;
    sed -i '9i\        ssl_certificate_key ../sslkey/2_cart.juanpi.com.key;' /usr/local/webserver/tengine/conf/vhosts/cart.juanpi.com.conf;
    ( /etc/init.d/tengine configtest && /etc/init.d/tengine reload && log_version $version && echo "cart 证书部署完成" ) || echo "cart 证书部署失败";
else
    echo "已经部署,无需重新部署"
fi
echo "cart 证书部署 结束..."
# ---------------------------- cart 证书部署 end  ---------------------------------


# ---------------------------- muser 证书部署 begin --------------------------------
echo "muser 证书部署 开始..."
version=2
check_version

if [ "$?" -lt "$version" ]
then
    echo "下载证书密钥......";
    sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/2/nginx/1_muser.juanpi.com_bundle.crt' -O /usr/local/webserver/tengine/sslkey/1_muser.juanpi.com_bundle.crt
    sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/2/nginx/2_muser.juanpi.com.key' -O /usr/local/webserver/tengine/sslkey/2_muser.juanpi.com.key
    echo "更改配置文件......"
    sed -i '7i\        include ssl.conf;' /usr/local/webserver/tengine/conf/vhosts/muser.juanpi.com.conf;
    sed -i '8i\        ssl_certificate ../sslkey/1_muser.juanpi.com_bundle.crt;' /usr/local/webserver/tengine/conf/vhosts/muser.juanpi.com.conf;
    sed -i '9i\        ssl_certificate_key ../sslkey/2_muser.juanpi.com.key;' /usr/local/webserver/tengine/conf/vhosts/muser.juanpi.com.conf;
    ( /etc/init.d/tengine configtest && /etc/init.d/tengine reload && log_version $version && echo "muser 证书部署完成" ) || echo "muser 证书部署失败";
else
    echo "已经部署,无需重新部署"
fi
echo "muser 证书部署 结束..."
# ---------------------------- muser 证书部署 end  ---------------------------------


# ---------------------------- 部署apcu begin --------------------------------
echo "部署apcu 开始..."
version=3
check_version

if [ "$?" -lt "$version" ]
then
    echo "下载apcu ..."
    cd ~ && wget https://pecl.php.net/get/apcu-4.0.10.tgz
    tar zxvf apcu-4.0.10.tgz
    cd apcu-4.0.10
    echo "开始安装..."
    (phpize && ./configure && make && make install && log_version $version &&
    sed -i 's/^opcache.save_comments.*/opcache.save_comments=1/g' /usr/local/webserver/php-5.6.12/etc/php.ini  &&
    sed -i 's/^opcache.load_comments.*/opcache.load_comments=1/g' /usr/local/webserver/php-5.6.12/etc/php.ini  &&
    sed -i '1008i\extension=apcu.so' /usr/local/webserver/php-5.6.12/etc/php.ini  &&
    echo -e "[apcu]\napc.enabled=1\napc.shm_segments=1\napc.shm_size=64M\napc.ttl=0\napc.enable_cli=0\n" >>/usr/local/webserver/php-5.6.12/etc/php.ini &&
    rm -rf apcu &&
    /etc/init.d/php-fpm-5612 test &&
    /etc/init.d/php-fpm-5612 reload &&
    log_version $version 
    ) || echo "编译安装失败"
else
    echo "已经部署,无需重新部署"
fi
echo "部署apcu  结束..."
# ---------------------------- 部署apcu end   --------------------------------



# ---------------------------- 部署act begin --------------------------------
echo "部署act 开始..."
version=4
check_version

if [ "$?" -lt "$version" ]
then
    echo "下载act ..."
    (rm /usr/local/webserver/tengine/conf/vhosts/s.juancdn.com.conf /usr/local/webserver/tengine/conf/vhosts/act.juancdn.com.conf -f &&
    sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/4/s.juancdn.com.conf' -O /usr/local/webserver/tengine/conf/vhosts/s.juancdn.com.conf &&
    sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/4/act.juancdn.com.conf' -O /usr/local/webserver/tengine/conf/vhosts/act.juancdn.com.conf &&
    /etc/init.d/tengine configtest && /etc/init.d/tengine reload && log_version $version 
    ) || echo "down load error"
else
    echo "已经部署,无需重新部署"
fi
echo "部署act  结束..."
# ---------------------------- 部署apcu end   --------------------------------

# ---------------------------- 部署act begin --------------------------------
echo "部署openresty 开始..."
version=5
check_version

if [ "$?" -lt "$version" ]
then
    if [ -f /tmp/openresty-1.9.7.4.7z ]; then
        rm /tmp/openresty-1.9.7.4.7z
    fi
    if [ -d /tmp/openresty-1.9.7.4 ]; then
        rm -rf /tmp/openresty-1.9.7.4
    fi
    echo "下载openresty ..."
    (sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/5/openresty-1.9.7.4.7z' -O /tmp/openresty-1.9.7.4.7z && cd /tmp &&
    7za x openresty-1.9.7.4.7z &&
    cd openresty-1.9.7.4/ &&
    sudo bash ./install.sh && log_version $version && cd ~ )|| echo "install error"
    if [ -f /tmp/openresty-1.9.7.4.7z ]; then
        rm /tmp/openresty-1.9.7.4.7z
    fi
    if [ -d /tmp/openresty-1.9.7.4 ]; then
        rm -rf /tmp/openresty-1.9.7.4
    fi
    yum install libuuid-devel -y
    
else
    echo "已经部署,无需重新部署"
fi
echo "部署openresty  结束..."
# ---------------------------- 部署openresty end   --------------------------------



# ----------------------------部署 s.juancdn.com begin  --------------------------------
echo "部署s.juancdn.com 开始..."
version=6
check_version

if [ "$?" -lt "$version" ]
then
    echo "下载 config ..."
    (sudo mv /usr/local/webserver/tengine/conf/vhosts/s.juancdn.com.conf /tmp/s.juancdn.com.conf && sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/6/s.juancdn.com.conf' -O /usr/local/webserver/tengine/conf/vhosts/s.juancdn.com.conf &&  log_version $version && cd ~ )|| echo "install error"
else
    echo "已经部署,无需重新部署"
fi
echo "部署s.juancdn.com  结束..."
# ---------------------------- 部署openresty end   --------------------------------

# ----------------------------部署 s.juancdn.com begin  --------------------------------
echo "部署s.juancdn.com 开始..."
version=7
check_version

if [ "$?" -lt "$version" ]
then
    echo "下载 config ..."
    (sudo mv /usr/local/webserver/openresty/nginx/conf/vhosts/s.juancdn.com.conf /tmp/s.juancdn.com.conf && sudo wget --no-check-certificate 'https://gitlab.com/zhuleilei/salt-vagrant-boxs/raw/master/pervision/6/s.juancdn.com.conf' -O /usr/local/webserver/openresty/nginx/conf/vhosts/s.juancdn.com.conf &&  log_version $version && cd ~ )|| echo "install error"
else
    echo "已经部署,无需重新部署"
fi
echo "部署s.juancdn.com  结束..."
# ---------------------------- 部署openresty end   --------------------------------
